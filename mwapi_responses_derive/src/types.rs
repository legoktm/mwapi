/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use proc_macro2::TokenStream as TokenStream2;
use quote::{format_ident, quote, ToTokens};
use serde::Deserialize;

/// Various types that can be represented in JSON
#[derive(Deserialize)]
#[cfg_attr(feature = "dbg", derive(Debug))]
#[serde(untagged)]
pub(crate) enum RustType {
    VecIdent { ident: String },
    Ident { ident: String },
    Complex(ComplexType),
    HashMap(HashMap),
    Simple(String),
}

impl ToTokens for RustType {
    fn to_tokens(&self, tokens: &mut TokenStream2) {
        match self {
            RustType::Complex(complex) => complex.to_tokens(tokens),
            RustType::HashMap(map) => map.to_tokens(tokens),
            RustType::Simple(inner) => {
                smart_ident(normalize_type(inner)).to_tokens(tokens)
            }
            RustType::VecIdent { ident } => {
                let ident = smart_ident(ident);
                let quoted = quote! { Vec<#ident> };
                quoted.to_tokens(tokens);
            }
            RustType::Ident { ident } => {
                let ident = smart_ident(ident);
                let quoted = quote! { #ident };
                quoted.to_tokens(tokens);
            }
        };
    }
}

/// A type that can optionally be wrapped in Vec<T> and Option<T>.
#[derive(Deserialize)]
#[cfg_attr(feature = "dbg", derive(Debug))]
pub(crate) struct ComplexType {
    pub(crate) inner: String,
    #[serde(default)]
    pub(crate) vec: bool,
    #[serde(default)]
    pub(crate) option: bool,
}

impl ToTokens for ComplexType {
    fn to_tokens(&self, tokens: &mut TokenStream2) {
        let mut type_ = smart_ident(normalize_type(&self.inner));
        if self.vec {
            type_ = quote! { Vec<#type_> };
        }
        if self.option {
            type_ = quote! { Option<#type_> };
        }
        type_.to_tokens(tokens);
    }
}

/// Represents HashMap<K, V>
#[derive(Deserialize)]
#[cfg_attr(feature = "dbg", derive(Debug))]
pub(crate) struct HashMap {
    pub(crate) key: String,
    pub(crate) value: String,
}

impl ToTokens for HashMap {
    fn to_tokens(&self, tokens: &mut TokenStream2) {
        let map = smart_ident("std::collections::HashMap");
        let key = format_ident!("{}", self.key);
        let value = smart_ident(&self.value);

        let quoted = quote! { #map<#key, #value> };
        quoted.to_tokens(tokens);
    }
}

/// Do some magic to create idents for complex things like "::foo::bar::Baz".
/// Also supports primitive types that should not be prefaced with ::.
fn smart_ident(input: &str) -> TokenStream2 {
    if input.contains("::") {
        let sp: Vec<_> = input
            .split("::")
            .map(|part| format_ident!("{}", part))
            .collect();
        let mut quoted = quote! {};
        for part in sp {
            quoted = quote! { #quoted::#part };
        }
        quoted
    } else {
        let ident = format_ident!("{}", input);
        quote! { #ident }
    }
}

/// Let us specify more specific types like "timestamp"
fn normalize_type(original: &str) -> &str {
    match original {
        "timestamp" => "mwapi_responses::timestamp::Timestamp",
        "expiry" => "mwapi_responses::timestamp::Expiry",
        "enum" => "String",
        val => val,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn stringify<P: ToTokens>(thing: P) -> String {
        let mut stream = quote! {};
        thing.to_tokens(&mut stream);
        stream.to_string()
    }

    #[test]
    fn test_smart_ident() {
        assert_eq!(
            smart_ident("std::collections::HashMap").to_string(),
            ":: std :: collections :: HashMap".to_string()
        );
        assert_eq!(smart_ident("u32").to_string(), "u32".to_string());
    }

    #[test]
    fn test_hashmap() {
        let map = RustType::HashMap(HashMap {
            key: "String".to_string(),
            value: "String".to_string(),
        });
        assert_eq!(
            stringify(map),
            ":: std :: collections :: HashMap < String , String >".to_string()
        );
    }

    #[test]
    fn test_complex() {
        let complex = RustType::Complex(ComplexType {
            inner: "String".to_string(),
            vec: true,
            option: true,
        });
        assert_eq!(stringify(complex), "Option < Vec < String > >".to_string())
    }

    #[test]
    fn test_simple() {
        assert_eq!(
            stringify(RustType::Simple("String".to_string())),
            "String".to_string()
        );
        assert_eq!(
            stringify(RustType::Simple("timestamp".to_string())),
            ":: mwapi_responses :: timestamp :: Timestamp".to_string()
        );
    }

    #[test]
    fn test_vecident() {
        let vecident = RustType::VecIdent {
            ident: "FooBar".to_string(),
        };
        assert_eq!(stringify(vecident), "Vec < FooBar >".to_string());
    }

    #[test]
    fn test_ident() {
        let ident = RustType::Ident {
            ident: "FooBar".to_string(),
        };
        assert_eq!(stringify(ident), "FooBar".to_string());
    }
}
