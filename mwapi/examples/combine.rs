/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use indexmap::IndexMap;
use mwapi::Client;
use tokio::time;
use tracing_subscriber::EnvFilter;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .init();
    let client = Client::new("https://en.wikipedia.org/w/api.php")
        .await
        .unwrap();
    let mut handles = IndexMap::new();
    for counter in 1..11 {
        let client = client.clone();
        let title = format!("Test_{}", counter);
        handles.insert(
            title.to_string(),
            tokio::spawn(async move {
                if counter > 4 {
                    time::sleep(time::Duration::from_secs(2)).await;
                }
                client
                    .get_value([
                        ("action", "query".to_string()),
                        ("titles", title),
                        ("prop", "info".to_string()),
                    ])
                    .await
            }),
        );
    }

    for (orig_title, handle) in handles {
        println!("awaiting on {}", &orig_title);
        let resp = handle.await.unwrap().unwrap();
        let recv_title = resp["query"]["pages"][0]["title"].as_str().unwrap();
        println!("Expected {}, got {}", orig_title, recv_title);
        dbg!(&resp);
    }
    drop(client);
    time::sleep(time::Duration::from_secs(2)).await;
    println!("all done");
}
