mwapi_responses
===============
[![crates.io](https://img.shields.io/crates/v/mwapi_responses.svg)](https://lib.rs/crates/mwapi_responses)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://gitlab.com/mwbot-rs/mwbot/badges/main/coverage.svg)](https://mwbot-rs.gitlab.io/mwbot/coverage/)


The `mwapi_responses` crate provides strict typing for dynamic MediaWiki Action
API queries. See the [documentation](https://docs.rs/mwapi_responses) ([main](https://mwbot-rs.gitlab.io/mwbot/mwapi_responses/))
for more details.

## License
`mwapi_responses` is (C) 2020-2021 Kunal Mehta <legoktm@debian.org> under the
GPL v3 or any later version.
