## 0.2.0 / 2022-11-03
* Update `externallinks` schema.
* Update `sea-query` dependency to new [0.27 release](https://www.sea-ql.org/blog/2022-10-31-whats-new-in-seaquery-0.27.0/).

## 0.1.2 / 2022-10-05
* Re-export `sea_query` for convenience of downstream users.

## 0.1.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.1.0 / 2022-09-19
* Initial release.
